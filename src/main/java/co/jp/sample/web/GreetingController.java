package co.jp.sample.web;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import co.jp.sample.domain.RequParams;
import co.jp.sample.service.Greeting;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@RestController
@RequestMapping(value = "/greeting")
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();


//    @RequestMapping(value="/greeting", 
//    		method = RequestMethod.POST, 
//    	    headers={"Accept=text/xml, application/json"}, 
//    		consumes = MediaType.APPLICATION_JSON_VALUE,
//    		produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public Greeting greeting(@RequestBody RequestParams param) {
//        return new Greeting(counter.incrementAndGet(),
//                            String.format(template, "a"));
//    }

    @RequestMapping(method = RequestMethod.POST)
    public Greeting greeting(@RequestBody RequParams param) {
        return new Greeting(param);
    }


}