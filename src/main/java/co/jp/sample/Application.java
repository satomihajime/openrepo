package co.jp.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
	
	public static ClassPathXmlApplicationContext context;
	
    public static void main(String[] args) {
    	SpringApplication.run(Application.class, args);
    	
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }
}