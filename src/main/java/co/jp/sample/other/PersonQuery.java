package co.jp.sample.other;

import javax.sql.DataSource;
 
import org.springframework.jdbc.core.JdbcTemplate;
 
public class PersonQuery {
    private DataSource dataSource = null;
 
    public void createTable() {
        final String sql = "create table person(id serial primary key, "
                + "first_name varchar(20), family_name varchar(20))";
        JdbcTemplate jt = new JdbcTemplate(this.dataSource);
        jt.execute(sql);
    }
 
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}