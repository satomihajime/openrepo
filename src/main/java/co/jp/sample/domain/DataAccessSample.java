package co.jp.sample.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class DataAccessSample {

	 @Autowired(required=false)
	 @Qualifier("jdbcTemplate1")
	 JdbcTemplate jdbcTemplate1;

	 @Autowired(required=false)
	 @Qualifier("jdbcTemplate2")
	 JdbcTemplate jdbcTemplate2;
	 
//	@Autowired(required=false)
//	@Qualifier("dataSourceWestZone1")
//	BasicDataSource ds;
	
	public DataAccessSample() {

	}

	@Transactional("transactionManager2")
	public void execute(String name, String age) {
		// do some work
		int result = 1;
		// List<Map<String, Object>> list =
		// jdbcTemplate.queryForList("select * from users");


		

		final String sql1 = "update users set password = ? where username = 'satomi'";
		result = jdbcTemplate1.update(sql1, new Object[] { age });

		final String sql2 = "update users set password = ? where username = 'satomi'";
		result = jdbcTemplate2.update(sql2, new Object[] { name });

		 String a = "";
		 String b = a.substring(4);

	}
}